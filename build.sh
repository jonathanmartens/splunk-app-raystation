#!/bin/sh

# Print usage
if test $# -ne 1; then
    echo "Usage: $(basename $0) PROJECT"
    exit 1
fi

# Test for write access
#TESTFN=`mktemp $1/.test-write-XXXXXX` 2> /dev/null
#if test x$TESTFN != x"" ; then
#    rm -f $TESTFN
#else
#    echo "$0: Can not write to destination, skipping.."
#    exit 0
#fi

# Define variables
GITTREEDIR=$1
GITREPOPATH="exported"

# Retrieve current version as defined in app
VERSION=$(echo -e $(grep version varian/default/app.conf | cut -d "=" -f 2))

# Tag
git tag $VERSION

# Retrieve source code version
SOURCE_VERSION=$(git describe --tags --dirty --always --long || git describe --tags --always --long || echo Unknown)

case "${SOURCE_VERSION}" in
    exported|Unknown)
        if ! grep -q Format $GITTREEDIR/EXPORTED_VERSION; then
            . $GITTREEDIR/EXPORTED_VERSION
            BRANCH=$(echo "${BRANCH}" | sed 's/ (\(.*, \)\{0,1\}\(.*\))/\2/')
        elif test -e $GITTREEDIR/VERSION ; then
            . $GITTREEDIR/VERSION
        fi
    ;;
    *)
        if [ -z "${BRANCH}" ]; then
            BRANCH=$(git branch --no-color | sed -e '/^[^\*]/d' -e 's/^\* //' -e 's/(no branch)/exported/')
        fi
    ;;
esac

# Update build number
sed -i "s/build\s=.*$/build = $SOURCE_VERSION/" $GITTREEDIR/default/app.conf

# Build
tar czf $GITTREEDIR-$SOURCE_VERSION.spl $(git ls-tree -r HEAD --name-only | grep $GITTREEDIR/) --exclude=$GITTREEDIR/local

# Reset changes as we do not want the build identifier in Git
git checkout -- $GITTREEDIR/default/app.conf

# Bump version number
VERSION_NEW=$(awk -F. '/[0-9]+\./{$NF++;print}' OFS=. <<< "$VERSION")

# Update version number
sed -i "s/version\s=\s$VERSION/version = $VERSION_NEW/" $GITTREEDIR/default/app.conf

# Commit
git add $GITTREEDIR/default/app.conf
git commit -m "[build] Bump version number to $VERSION_NEW" 

# Push
git push
